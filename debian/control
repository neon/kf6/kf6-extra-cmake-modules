Source: kf6-extra-cmake-modules
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libjs-jquery,
               libjs-underscore,
               pkg-kde-tools-neon,
               python3,
               python3-distutils-extra,
               python3-setuptools,
               python3-sphinx,
               python3-sphinxcontrib.qthelp,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-tools-dev,
               sphinx-common
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/kdesupport/extra-cmake-modules
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/extra-cmake-modules
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/extra-cmake-modules.git

Package: kf6-extra-cmake-modules
Architecture: any
Depends: python3:any, ${misc:Depends}, 
         ${shlibs:Depends}, 
         ${sphinxdoc:Depends}
Breaks: extra-cmake-modules (<< 6)
Replaces: extra-cmake-modules,
          extra-cmake-modules-doc,
          kf6-extra-cmake-modules-doc,
Description: Extra modules and scripts for CMake
 Extra CMake Modules, or ECM, aims to augment CMake with additional modules. It
 serves as both a staging ground for new modules before they are moved upstream
 to CMake and a place for modules that, for whatever reason, are not a good fit
 for CMake itself.
 .
 It is primarily driven by the needs of the KDE community, but it is hoped that
 other projects will find it useful, and outside contributions are always
 welcome. The main rule for new modules is that there must be at least two
 downstream users.

Package: kf6-extra-cmake-modules-doc
Architecture: all
Depends: kf6-extra-cmake-modules, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: extra-cmake-modules
Architecture: all
Depends: kf6-extra-cmake-modules, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: extra-cmake-modules-doc
Architecture: all
Depends: kf6-extra-cmake-modules, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
